<?php namespace herron\command;

use herron\controller\Request;


class DefaultCommand extends Command
{
    function executeRequest(Request $request) {
        //No specific command was given, so we just default to the main search page
        $request->setMessage("Default search page loaded");
        include($_SERVER["DOCUMENT_ROOT"] . "/src/php/herron/view/search.php");
    }
}

?>
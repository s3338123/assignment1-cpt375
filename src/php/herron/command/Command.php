<?php  namespace herron\command;

use herron\controller\Request;


abstract class Command
{
    //A command class is restricted from having constructor arguments
    final function __construct() {}

    function execute(Request $request) {
        $this->executeRequest($request);
        $request->setCommand($this);
    }

    abstract function executeRequest(Request $request);
}

?>
<?php namespace herron\command;

use herron\controller\Request;
use herron\domain\Region;
use herron\mapper\PersistenceFactory;


class ListRegions extends Command
{
    function executeRequest(Request $request) {
        $finder = PersistenceFactory::getFinder(Region::class);
        //Process any request parameters against the query function (an empty query acts as a 'SELECT *'
        $query = $finder->query();
        $collection = $finder->find($query);

        $request->setObject("regions", $collection);
    }
}
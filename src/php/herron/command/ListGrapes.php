<?php namespace herron\command;

use herron\controller\Request;
use herron\domain\Grape;
use herron\mapper\PersistenceFactory;


class ListGrapes extends Command
{
    function executeRequest(Request $request) {
        $finder = PersistenceFactory::getFinder(Grape::class);
        //Process any request parameters against the query function (an empty query acts as a 'SELECT *'
        $query = $finder->query();
        $collection = $finder->find($query);

        $request->setObject("grapes", $collection);
    }
}
<?php namespace herron\command;

use herron\controller\Request;


class CommandFactory
{
    private static $commands = array();

    static public function getCommand($command) {
        //$command = $request->getProperty("command");

        if(isset($commands[$command])) {
            return self::$commands[$command];
        }

        switch($command) {
            case "getWines":
                self::$commands[$command] = new ListWines();
                break;
            case "getRegions":
                self::$commands[$command] = new ListRegions();
                break;
            case "getGrapes":
                self::$commands[$command] = new ListGrapes();
                break;
            case "getYears":
                self::$commands[$command] = new ListYears();
                break;
            default:
                return new DefaultCommand();
        }

        return self::$commands[$command];
    }
}
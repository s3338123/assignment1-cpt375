<?php namespace herron\command;

use herron\controller\Request;
use herron\mapper\PersistenceFactory;
use herron\domain\Wine;


class ListYears extends Command
{
    function executeRequest(Request $request) {
        $finder = PersistenceFactory::getFinder(Wine::class);
        $query = $finder->query();
        $collection = $finder->find($query);

        $years = array();
        // Add the year range from the existing wine data (uniques only)
        foreach($collection as $wine) {
            if(!in_array($wine->getYear(), $years)) {
                array_push($years, $wine->getYear());
            }
        }
        //sort collection by year before adding it back to the request object
        sort($years);
        $request->setObject("years", $years);
    }
}
<?php namespace herron\command;

use herron\controller\Request;
use herron\domain\Wine;
use herron\mapper\Assembler;
use herron\mapper\PersistenceFactory;


class ListWines extends Command
{
    function executeRequest(Request $request) {
        $finder = PersistenceFactory::getFinder(Wine::class);
        //Process any request parameters against the query function (an empty query acts as a 'SELECT *'
        $query = $this->buildQuery($finder, $request);
        $collection = $finder->find($query);
        $request->setObject("wines", $collection);
    }

    private function buildQuery(Assembler $finder, Request $request) {
        $wineName = $request->getProperty("wine_name");
        $wineryName = $request->getProperty("winery_name");
        $region = $request->getProperty("region");
        $grape = $request->getProperty("grape");
        $minYear = $request->getProperty("year_from");
        $maxYear = $request->getProperty("year_to");
        $minStock = $request->getProperty("min_stock");
        $minSold = $request->getProperty("min_sold");
        $minCost = $request->getProperty("min_cost");
        $maxCost = $request->getProperty("max_cost");

        //Fine tune any adjustments before finalising the query
        $region = $region == "All" ? "" : $region;

        return $finder->query()
            ->field('wine_name')->like($wineName)
            ->field("winery_name")->like($wineryName)
            ->field("region_name")->eq($region)
            ->field("variety")->eq($grape)
            ->field("year")->gte($minYear)->lte($maxYear)
            ->field("on_hand")->gte($minStock)
            ->field("sold")->gte($minSold)
            ->field("cost")->gte($minCost)->lte($maxCost);
    }
}
<?php namespace herron\controller;

use Exception;
use herron\command\CommandFactory;


class AnswerController extends PageController
{
    function process() {
        try {
            // Check field input for errors (this will determine which page to forward to as well)
            $this->validateFields();
            CommandFactory::getCommand("getWines")->execute($this->getRequest());
            $this->forward("results.php");
        }
        catch(Exception $e) {
            //TODO: forward to a different page
        }
    }

    private function validateFields() {
        $request = $this->getRequest();
        $wineName = $request->getProperty("wine_name");
        $wineryName = $request->getProperty("winery_name");
        $region = $request->getProperty("region");
        $grape = $request->getProperty("grape");
        $minYear = $request->getProperty("year_from");
        $maxYear = $request->getProperty("year_to");
        $minStock = $request->getProperty("min_stock");
        $minSold = $request->getProperty("min_sold");
        $minCost = $request->getProperty("min_cost");
        $maxCost = $request->getProperty("max_cost");


    }
}
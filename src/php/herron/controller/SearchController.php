<?php namespace herron\controller;

use Exception;
use herron\command\CommandFactory;


class SearchController extends PageController
{
    function process() {
        try {
            CommandFactory::getCommand("getRegions")->execute($this->getRequest());
            CommandFactory::getCommand("getGrapes")->execute($this->getRequest());
            CommandFactory::getCommand("getYears")->execute($this->getRequest());
        }
        catch(Exception $e) {
            //TODO: forward to a different page
        }
    }

    function getAllRegions() {
        return $this->getRequest()->getObject("regions");
    }

    function getAllGrapes() {
        return $this->getRequest()->getObject("grapes");
    }

    function getAllYears() {
        return $this->getRequest()->getObject("years");
    }

    function optionWrap($data) {
        return "<option>{$data}</option>";
    }
}
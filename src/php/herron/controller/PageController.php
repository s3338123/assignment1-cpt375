<?php namespace herron\controller;

use herron\base\ApplicationRegistry;


abstract class PageController
{
    abstract function process();

    function forward($page) {
        include($page);
        exit(0);
    }

    function getRequest() {
        return ApplicationRegistry::getRequest();
    }
}
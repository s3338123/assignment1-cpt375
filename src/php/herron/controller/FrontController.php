<?php namespace herron\controller;

use herron\base\ApplicationRegistry;
use herron\command\CommandFactory;


class FrontController
{
    private function __construct() {}

    static function run() {
        $instance = new FrontController();
        $instance->init();
        $instance->handleRequest();
    }

    function init() {
        //no-op
    }

    function handleRequest() {
        $request = ApplicationRegistry::getRequest();
        $commandFactory = new CommandFactory();
        $command = $commandFactory->getCommand($request);
        $command->execute($request);
    }
}

?>
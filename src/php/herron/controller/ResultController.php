<?php namespace herron\controller;

use herron\domain\Wine;


class ResultController extends PageController
{
    private $rows = array();

    function process() {
        $this->rows = $this->getRequest()->getObject("wines");
    }

    public function getRows() {
        return $this->rows;
    }

    public function getRowData(Wine $row) {
        return  "<tr>
                    <td> {$row->getName()} </td>
                    <td> {$row->getGrape()} </td>
                    <td> {$row->getYear()} </td>
                    <td> {$row->getWinery()} </td>
                    <td> {$row->getRegion()} </td>
                    <td> {$row->getCost()} </td>
                    <td> {$row->getStock()} </td>
                    <td> {$row->getAmountSold()} </td>
                    <td> {$row->getSalesRevenue()} </td>
                </tr>";
    }
}
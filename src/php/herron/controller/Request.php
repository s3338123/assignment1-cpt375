<?php namespace herron\controller;

use herron\command\Command;


class Request
{
    private $properties;
    private $objects = array();
    private $message = array();
    private $lastCommand;

    function __construct() {
        $this->init();
    }

    function init() {
        if(isset($_SERVER['REQUEST_METHOD'])) {
            $this->properties = $_REQUEST;
            return;
        }

        foreach($_SERVER['argv'] as $arg) {
            if(strpos($arg, '=')) {
                list($key, $val) = explode('=', $arg);
                $this->setProperty($key, $val);
            }
        }
    }

    function setCommand(Command $command) {
        $this->lastCommand = $command;
    }

    function setObject($name, $object) {
        $this->objects[$name] = $object;
    }

    function setProperty($key, $val) {
        $this->properties[$key] = $val;
    }

    function setMessage($message) {
        array_push($this->message, $message);
    }

    function getObject($name) {
        if(isset($this->objects[$name])) {
            return $this->objects[$name];
        }

        return null;
    }

    function getProperty($key) {
        if(isset($this->properties[$key])) {
            return $this->properties[$key];
        }

        return null;
    }

    function getMessage() {
        return $this->message;
    }

    function getMessageString($separator='\n') {
        return implode($separator, $this->message);
    }

    function getLastCommand() {
        return $this->lastCommand;
    }

    function clearLastCommand() {
        $this->lastCommand = null;
    }
}

?>
<?php namespace herron\mapper;


class GrapeIdentityObject extends IdentityObject
{
    private $supportedFields = array (
        'variety_id',
        'variety'
    );

    function __construct($field = null) {
        parent::__construct($field, $this->supportedFields);
    }
}

?>
<?php namespace herron\mapper;


class WineSelectionFactory extends SelectionFactory
{
    function selection(IdentityObject $object) {
        $fields = implode(',', $object->getObjectFields());
        $select =  "SELECT $fields FROM winestore.wine
                    JOIN winestore.wine_variety ON wine.wine_id = wine_variety.wine_id
                    JOIN winestore.grape_variety ON wine_variety.variety_id = grape_variety.variety_id
                    JOIN winestore.winery ON wine.winery_id = winery.winery_id
                    JOIN winestore.region ON winery.region_id = region.region_id
                    JOIN winestore.inventory ON wine.wine_id = inventory.wine_id
                    JOIN (
                      SELECT items.wine_id, SUM(items.price) AS revenue, SUM(items.qty) AS sold
                        FROM winestore.items
                        GROUP BY items.wine_id
                    ) p ON p.wine_id = wine.wine_id";

        $groupBy = "GROUP BY wine.wine_id";
        $orderBy = "ORDER BY wine.wine_name";
        list($where, $values) = $this->buildWhere($object);

        return array($select . " " . $where . " " . $groupBy . " " . $orderBy, $values);
    }
}

?>
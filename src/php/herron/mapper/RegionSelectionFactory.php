<?php namespace herron\mapper;


class RegionSelectionFactory extends SelectionFactory
{
    function selection(IdentityObject $object) {
        $fields = implode(',', $object->getObjectFields());
        $select = "SELECT $fields FROM winestore.region";
        list($where, $values) = $this->buildWhere($object);

        $orderBy = "ORDER BY region.region_name";

        return array($select . " " . $where . " " . $orderBy, $values);
    }
}

?>
<?php namespace herron\mapper;

use herron\base\AppException;
use herron\base\ApplicationRegistry;
use herron\domain\IdentityMap;
use PDO;


class Assembler
{
    protected static $PDO;
    //private $factory = null;
    private $statements = array();

    function __construct(PersistenceFactory $factory) {
        $this->factory = $factory;
        if(!isset(self::$PDO)) {
            $dsn = ApplicationRegistry::getDSN();
            $user = ApplicationRegistry::getDBUser();
            $password = ApplicationRegistry::getDBPassword();

            if(is_null($dsn)) {
                throw new AppException("Invalid or missing DSN");
            }

            self::$PDO = new PDO($dsn, $user, $password);
            self::$PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
    }

    function getStatement($selectString) {
        if (!isset($this->statements[$selectString])) {
            $this->statements[$selectString] = self::$PDO->prepare($selectString);
        }

        return $this->statements[$selectString];
    }

    function findOne(IdentityObject $object) {
        return $this->find($object, true);
    }

    function find(IdentityObject $object, $returnFirstRow = false) {
        $selectionFactory = $this->factory->getSelectionFactory();
        list($selection, $values) = $selectionFactory->selection($object);

        // Check identity map to see if we've run this search before
        $current = IdentityMap::exists($object, $selection, $values);
        if(!is_null($current)) {
            return $current;
        }

        $statement = $this->getStatement($selection);
        $statement->execute($values);
        $dbData = $returnFirstRow ? array(0 => $statement->fetch()) : $statement->fetchAll();

        $domainObjectCollection = $this->factory->getCollection($dbData);

        // Cache results in identity map
        IdentityMap::add($object, $selection, $values, $domainObjectCollection);

        return $domainObjectCollection;
    }

    function query() {
        return $this->factory->getIdentityObject();
    }
}
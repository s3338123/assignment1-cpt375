<?php namespace herron\mapper;

use herron\domain\DomainObject;


abstract class Collection implements \IteratorAggregate
{
    protected $factory;
    protected $total = 0;
    protected $domainData = array();

    private $domainObjects = array();

    function __construct(array $domainData = null, DomainObjectFactory $factory = null) {
        if(!is_null($domainData) && !is_null($factory)) {
            $this->domainData = $domainData;
            $this->total = count($domainData);
        }

        $this->factory = $factory;
    }

    function add(DomainObject $object) {
        $class = $this->targetClass();
        if(!($object instanceof $class)) {
            throw new \Exception("Collection is of incorrect type {$class}");
        }

        //$this->notify();
        $this->domainObjects[$this->total] = $object;
        $this->total++;
    }

    function getIterator() {
        for ($i = 0; $i < $this->total; $i++) {
            yield($this->getRow($i));
        }
    }

    abstract function targetClass();

    protected function notify() {
        //no-op
    }

    private function getRow($num) {
        //$this->notify();
        if($num >= $this->total || $num < 0) {
            return null;
        }

        if(isset($this->domainObjects[$num])) {
            return $this->domainObjects[$num];
        }

        if(isset($this->domainData[$num])) {
            $this->domainObjects[$num] = $this->factory->createObject($this->domainData[$num]);

            return $this->domainObjects[$num];
        }

        return null;
    }
}

?>
<?php namespace herron\mapper;


class GrapeSelectionFactory extends SelectionFactory
{
    function selection(IdentityObject $object) {
        $fields = implode(',', $object->getObjectFields());
        $select = "SELECT $fields FROM winestore.grape_variety";
        list($where, $values) = $this->buildWhere($object);

        $orderBy = "ORDER BY grape_variety.variety";

        return array($select . " " . $where . " " . $orderBy, $values);
    }
}

?>
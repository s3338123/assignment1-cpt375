<?php namespace herron\mapper;


abstract class SelectionFactory
{
    abstract function selection(IdentityObject $object);

    function buildWhere(IdentityObject $object) {
        if($object->isVoid()) {
            return array("", array());
        }

        $comparisonStrings = array();
        $values = array();

        foreach($object->getComparisons() as $comparison) {
            if(empty($comparison['value'])) {
                continue;
            }
            $comparisonStrings[] = "{$comparison['name']} {$comparison['operator']} ?";
            $values[] = $comparison['value'];
        }

        if($values) {
            $where = "WHERE " . implode(" AND ", $comparisonStrings);
            return array($where, $values);
        }

        return array("", $values);
    }
}
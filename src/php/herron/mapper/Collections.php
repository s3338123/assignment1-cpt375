<?php namespace herron\mapper;

use herron\domain\Grape;
use herron\domain\Region;
use herron\domain\Wine;


class WineCollection extends Collection implements \herron\domain\WineCollection
{
    function targetClass() {
        return Wine::class;
    }
}

class RegionCollection extends Collection implements \herron\domain\RegionCollection
{
    function targetClass() {
        return Region::class;
    }
}

class GrapeCollection extends Collection implements \herron\domain\GrapeCollection
{
    function targetClass() {
        return Grape::class;
    }
}

?>
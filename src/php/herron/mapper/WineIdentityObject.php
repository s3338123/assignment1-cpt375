<?php namespace herron\mapper;


class WineIdentityObject extends IdentityObject
{
    private $supportedFields = array(
        'wine.wine_id',
        'wine_name',
        'variety',
        'year',
        'winery_name',
        'region_name',
        'cost',
        'on_hand',
        'sold',
        'revenue'
    );

    function __construct($field = null) {
        parent::__construct($field, $this->supportedFields);
    }
}

?>
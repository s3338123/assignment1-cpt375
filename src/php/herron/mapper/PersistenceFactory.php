<?php namespace herron\mapper;

use herron\base\AppException;
use herron\domain\Region;
use herron\domain\Wine;
use herron\domain\Grape;



abstract class PersistenceFactory
{
    private static $factories = array();

    abstract function getDomainObjectFactory();
    abstract function getCollection(array $array);
    abstract function getSelectionFactory();
    abstract function getIdentityObject();

    static function getFactory($factoryType) {
        if(isset($factories[$factoryType])) {
            return $factories[$factoryType];
        }

        switch($factoryType) {
            case Wine::class:
                self::$factories[$factoryType] = new WinePersistenceFactory();
                break;
            case Region::class:
                self::$factories[$factoryType] = new RegionPersistenceFactory();
                break;
            case Grape::class:
                self::$factories[$factoryType] = new GrapePersistenceFactory();
                break;
            default:
                throw new AppException("Invalid PersistenceFactory requested: $factoryType");
        }

        return self::$factories[$factoryType];
    }

    // Simple convenience method to retrieve DomainObject Assembler
    static function getFinder($finderType) {
        return new Assembler(self::getFactory($finderType));
    }
}

class WinePersistenceFactory extends PersistenceFactory
{
    function getDomainObjectFactory() {
        return new WineObjectFactory();
    }

    function getCollection(array $array) {
        return new WineCollection($array, $this->getDomainObjectFactory());
    }

    function getSelectionFactory() {
        return new WineSelectionFactory();
    }

    function getIdentityObject() {
        return new WineIdentityObject();
    }
}

class RegionPersistenceFactory extends PersistenceFactory
{
    function getDomainObjectFactory() {
        return new RegionObjectFactory();
    }

    function getCollection(array $array) {
        return new RegionCollection($array, $this->getDomainObjectFactory());
    }

    function getSelectionFactory() {
        return new RegionSelectionFactory();
    }

    function getIdentityObject() {
        return new RegionIdentityObject();
    }
}

class GrapePersistenceFactory extends PersistenceFactory
{
    function getDomainObjectFactory() {
        return new GrapeObjectFactory();
    }

    function getCollection(array $array) {
        return new GrapeCollection($array, $this->getDomainObjectFactory());
    }

    function getSelectionFactory() {
        return new GrapeSelectionFactory();
    }

    function getIdentityObject() {
        return new GrapeIdentityObject();
    }
}

?>
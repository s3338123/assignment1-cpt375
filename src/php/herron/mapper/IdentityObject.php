<?php namespace herron\mapper;

use Exception;

class Field
{
    protected $name = null;
    protected $operator = null;
    protected $comparisons = array();
    protected $incomplete = false;

    function __construct($name) {
        $this->name = $name;
    }

    function addTest($operator, $value) {
        $this->comparisons[] = array('name' => $this->name, 'operator' => $operator, 'value' => $value);
    }

    function getComparisons() {
        return $this->comparisons;
    }

    function isIncomplete() {
        return empty($this->comparisons);
    }
}

class IdentityObject
{
    protected $currentField = null;
    protected $fields = array();
    private $enforce = array();

    function __construct($field = null, array $enforce = null) {
        if(!is_null($enforce)) {
            $this->enforce = $enforce;
        }

        if(!is_null($field)) {
            $this->field($field);
        }
    }

    function getObjectFields() {
        return $this->enforce;
    }

    function field($fieldName) {
        if(!$this->isVoid() && $this->currentField->isIncomplete()) {
            throw new Exception("Incomplete Field");
        }

        $this->enforceField($fieldName);

        if(isset($this->fields[$fieldName])) {
            $this->currentField = $this->fields[$fieldName];
        }
        else {
            $this->currentField = new Field($fieldName);
            $this->fields[$fieldName] = $this->currentField;
        }

        return $this;
    }

    function isVoid() {
        return empty($this->fields);
    }

    function enforceField($fieldName) {
        if(!in_array($fieldName, $this->enforce) && !empty($this->enforce)) {
            $enforceList = implode(', ', $this->enforce);
            throw new Exception("{$fieldName} is not a legal field. Valid fields are: $enforceList");
        }
    }

    // We list each of our operators for building WHERE clauses below
    function like($value) {
        return empty($value) ?
            $this->operator("LIKE", $value) :
            $this->operator("LIKE", "%" . $value . "%");
    }

    function eq($value) {
        return $this->operator("=", $value);
    }

    function lt($value) {
        return $this->operator("<", $value);
    }

    function lte($value) {
        return $this->operator("<=", $value);
    }

    function gt($value) {
        return $this->operator(">", $value);
    }

    function gte($value) {
        return $this->operator(">=", $value);
    }

    private function operator($symbol, $value) {
        if($this->isVoid()) {
            throw new Exception("No object field has been defined");
        }

        $this->currentField->addTest($symbol, $value);

        return $this;
    }

    function getComparisons() {
        $comparisons = array();
        foreach($this->fields as $field) {
            $comparisons = array_merge($comparisons, $field->getComparisons());
        }

        return $comparisons;
    }
}

?>
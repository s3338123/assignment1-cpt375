<?php namespace herron\mapper;


class RegionIdentityObject extends IdentityObject
{
    private $supportedFields = array (
        'region_id',
        'region_name'
    );

    function __construct($field = null) {
        parent::__construct($field, $this->supportedFields);
    }
}

?>
<?php namespace herron\mapper;

use herron\base\AppException;
use herron\domain\Grape;
use herron\domain\Region;
use herron\domain\Wine;


abstract class DomainObjectFactory
{
    abstract function createObject(array $array);
}

class WineObjectFactory extends DomainObjectFactory
{
    function createObject(array $array) {
        try {
            $wineObject = new Wine($array['wine_id'], $array['wine_name']);

            $wineObject->setGrape($array['variety']);
            $wineObject->setYear($array['year']);
            $wineObject->setWinery($array['winery_name']);
            $wineObject->setRegion($array['region_name']);
            $wineObject->setCost($array['cost']);
            $wineObject->setStock($array['on_hand']);
            $wineObject->setAmountSold($array['sold']);
            $wineObject->setSalesRevenue($array['revenue']);

            return $wineObject;
        }
        catch(\Exception $e) {
            throw new AppException("Invalid Wine object parameters supplied", $e);
        }
    }
}

class RegionObjectFactory extends DomainObjectFactory
{
    function createObject(array $array) {
        try {
            $regionObject = new Region($array['region_id'], $array['region_name']);

            return $regionObject;
        }
        catch(\Exception $e) {
            throw new AppException("Invalid Region object parameters supplied", $e);
        }
    }
}

class GrapeObjectFactory extends DomainObjectFactory
{
    function createObject(array $array) {
        try {
            $grapeObject = new Grape($array['variety_id'], $array['variety']);

            return $grapeObject;
        }
        catch(\Exception $e) {
            throw new AppException("Invalid Grape object parameters supplied", $e);
        }
    }
}

?>
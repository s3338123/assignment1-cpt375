<?php namespace herron\domain;


class Grape extends DomainObject
{
    private $name;

    function __construct($id = null, $name = null) {
        $this->name = $name;
        parent::__construct($id);
    }

    function setName($name) {
        $this->name = $name;
    }

    function getName() {
        return $this->name;
    }
}

?>
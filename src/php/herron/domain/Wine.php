<?php namespace herron\domain;


class Wine extends DomainObject
{
    private $name;
    private $grape;
    private $year;
    private $winery;
    private $region;
    private $cost;
    private $stock;
    private $amountSold;
    private $salesRevenue;

    function __construct($id = null, $name = null) {
        $this->name = $name;
        parent::__construct($id);
    }

    function setName($name) {
        $this->name = $name;
    }

    function setGrape($grape) {
        $this->grape = $grape;
    }

    function setYear($year) {
        $this->year = $year;
    }

    function setWinery($winery) {
        $this->winery = $winery;
    }

    function setRegion($region) {
        $this->region = $region;
    }

    function setCost($cost) {
        $this->cost = $cost;
    }

    function setStock($stock) {
        $this->stock = $stock;
    }

    function setAmountSold($amountSold) {
        $this->amountSold = $amountSold;
    }

    function setSalesRevenue($salesRevenue) {
        $this->salesRevenue = $salesRevenue;
    }

    function getName() {
        return $this->name;
    }

    function getGrape() {
        return $this->grape;
    }

    function getYear() {
        return $this->year;
    }

    function getWinery() {
        return $this->winery;
    }

    function getRegion() {
        return $this->region;
    }

    function getCost() {
        return $this->cost;
    }

    function getStock() {
        return $this->stock;
    }

    function getAmountSold() {
        return $this->amountSold;
    }

    function getSalesRevenue() {
        return $this->salesRevenue;
    }
}

?>
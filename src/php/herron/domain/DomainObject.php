<?php namespace herron\domain;


abstract class DomainObject
{
    private $id = -1;

    function __construct($id = null) {
        if(!is_null($id)) {
            $this->id = $id;
        }
    }

    function setId($id) {
        $this->id = $id;
    }

    function getId() {
        return $this->id;
    }
}

?>
<?php namespace herron\domain;

interface Collection extends \IteratorAggregate
{
    function add(DomainObject $domainObject);
}

interface WineCollection extends Collection {}

interface RegionCollection extends Collection {}

interface GrapeCollection extends Collection {}

?>
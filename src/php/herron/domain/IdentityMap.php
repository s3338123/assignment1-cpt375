<?php namespace herron\domain;

use herron\mapper\Collection;
use herron\mapper\IdentityObject;


class IdentityMap
{
    //private $objectInventory = array();
    private static $instance = null;

    private function __construct() {}

    static function instance() {
        if (is_null(self::$instance)) {
            self::$instance = new IdentityMap();
        }

        return self::$instance;
    }

    static function add(IdentityObject $object, $selection, $criteria, Collection $domainObjects) {
        // Fairly crude attempt at creating a unique key, should be good enough for current requirements.
        // N.B. The most obvious issue here will be queries that are the same, but where the WHERE clause has been
        // called in a different order. The DB result will be exactly the same, but the keys generated below will not
        // match, so duplicates will be created.
        $key = implode(".", array(get_class($object), $selection, implode(".", $criteria)));
        \apc_store($key, $domainObjects);
    }

    static function exists(IdentityObject $object, $selection, $criteria) {
        $key = implode(".", array(get_class($object), $selection, implode(".", $criteria)));
        $success = false;
        $object = \apc_fetch($key, $success);

        if($success && $object instanceof Collection) {
            return $object;
        }

        return null;
    }
}

?>
<?php namespace herron\base;

use herron\controller\Request;

require_once($_SERVER["DOCUMENT_ROOT"] . "/src/php/herron/db.php");


abstract class Registry
{
    abstract protected function get($key);
    abstract protected function set($key, $val);
}

class ApplicationRegistry extends Registry
{
    private static $instance = null;
    private $request = null;

    private function __construct() {}

    private function clean() {
        self::$instance = null;
    }

    static function instance() {
        if (is_null(self::$instance)) {
            self::$instance = new self();
            self::$instance->init();
        }

        return self::$instance;
    }

    function init() {
        $dsn = DB_ENGINE . ":host=" . DB_HOST . ";dbname=" . DB_NAME;
        $this->setDSN($dsn);
        $this->set("dbUser", DB_USER);
        $this->set("dbPass", DB_PW);
    }

    protected function get($key) {
        return \apc_fetch($key);
    }

    protected function set($key, $val) {
        return \apc_store($key, $val);
    }

    static function getDSN() {
        return self::instance()->get("dsn");
    }

    static function getDBUser() {
        return self::instance()->get("dbUser");
    }

    static function getDBPassword() {
        return self::instance()->get("dbPass");
    }

    static function setDSN($dsn) {
        self::instance()->set("dsn", $dsn);
    }

    static function getRequest() {
        $instance = self::instance();

        if(is_null($instance->request)) {
            $instance->request = new Request();
        }

        return $instance->request;
    }
}

?>
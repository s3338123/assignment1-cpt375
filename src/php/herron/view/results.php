<?php namespace herron\view;
$controller = new \herron\controller\ResultController();
$controller->process();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Super Search Wine-o-Rama</title>

    <!-- Bootstrap -->
    <link href="/src/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="row">
        <h1>Search Results</h1>
        <table class="table table-bordered table-striped table-condensed">
            <thead>
                <tr>
                    <th>Wine Name</th>
                    <th>Grape</th>
                    <th>Year</th>
                    <th>Winery</th>
                    <th>Region</th>
                    <th>Minimum Cost $</th>
                    <th>Stock On Hand</th>
                    <th>Bottles Ordered</th>
                    <th>Revenue $</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($controller->getRows() as $row) { echo $controller->getRowData($row); } ?>
            </tbody>
        </table>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/src/js/bootstrap.min.js"></script>
</body>
</html>

<?php namespace herron\view;
$controller = new \herron\controller\SearchController();
$controller->process();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Super Search Wine-o-Rama</title>

    <!-- Bootstrap -->
    <link href="/src/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="container">
    <div class="row">
        <h1>Search Wines</h1>
        <form class="form-horizontal" action="/src/php/herron/view/answer.php" method="get">
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputWine">Wine Name</label>
                <div class="col-sm-8">
                    <input type="text" name="wine_name" class="form-control" id="inputWine" placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputWinery">Winery Name</label>
                <div class="col-sm-8">
                    <input type="text" name="winery_name" class="form-control" id="inputWinery" placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputRegion">Region</label>
                <div class="col-sm-8">
                    <select class="form-control" name="region" id="inputRegion">
                        <option selected="selected"></option>
                        <?php
                            foreach($controller->getAllRegions() as $region) {
                                echo $controller->optionWrap($region->getName());
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputGrape">Grape</label>
                <div class="col-sm-8">
                    <select class="form-control" name="grape" id="inputGrape">
                        <option selected="selected"></option>
                        <?php
                            foreach($controller->getAllGrapes() as $grape) {
                                echo $controller->optionWrap($grape->getName());
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputYearFrom">Year</label>
                <div class="col-sm-8">
                    <select class="form-control" name="year_from" id="inputYearFrom">
                        <option selected="selected"></option>
                        <?php
                            foreach($controller->getAllYears() as $year) {
                                echo $controller->optionWrap($year);
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputYearTo">Year</label>
                <div class="col-sm-8">
                    <select class="form-control" name="year_to" id="inputYearTo">
                        <option selected="selected"></option>
                        <?php
                            foreach($request->getObject("years") as $year) {
                                echo $controller->optionWrap($year);
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputMinStock">Minimum Stock</label>
                <div class="col-sm-8">
                    <input type="text" name="min_stock" class="form-control" id="inputMinStock" placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputMinSold">Minimum Sold</label>
                <div class="col-sm-8">
                    <input type="text" name="min_sold" class="form-control" id="inputMinSold" placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputMinCost">Cost From</label>
                <div class="col-sm-8">
                    <input type="text" name="min_cost" class="form-control" id="inputCostFrom" placeholder="">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" for="inputMaxCost">Cost To</label>
                <div class="col-sm-8">
                    <input type="text" name="max_cost" class="form-control" id="inputCostEnd" placeholder="">
                </div>
            </div>
            <div class="text-center">
                <input type="submit" class="btn btn-default" />
            </div>
            <input type="hidden" name="submitted" value="yes" />
        </form>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/src/js/bootstrap.min.js"></script>
</body>
</html>
